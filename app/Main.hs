{-# LANGUAGE DataKinds #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE PolyKinds #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeOperators #-}

module Main where

import Commands
import Control.Monad (foldM)
import Data
import Data.Either (fromRight, lefts)
import Data.Maybe (fromMaybe)
import qualified Data.Text as T
import GHC.IO.Encoding (setLocaleEncoding, utf8)
import Interpreters
import Polysemy
import System.Exit (ExitCode (..), exitWith)
import Utils

versionFileNames :: [Text]
versionFileNames = [".nvmrc", ".node-version"]

envVarCmdName :: Text
envVarCmdName = "SWITCH_NODE_CMD"

defaultChangeCommand :: Text
defaultChangeCommand = "n {{version}}"

-- TODO: try Error + runError/errorToIOFinal from polysemy
program :: Sem '[VersionFile, Node, Console, Environment, Embed IO] ExitCode
program = do
  desiredVersions <- mapM readVersionFromVersionFileC versionFileNames
  r <- processDesiredVersions desiredVersions
  case r of
    Right _ -> return ExitSuccess
    Left err -> do
      printErrorC $ "❌ Error occurred:\n" <> err
      return $ ExitFailure 2
  where
    processDesiredVersions :: [Either Text NodeVersion] -> Sem '[VersionFile, Node, Console, Environment, Embed IO] (Either Text ())
    processDesiredVersions desiredVersions = do
      let desiredVersion = firstRight desiredVersions
      case desiredVersion of
        Nothing -> return $ Left $ "Failed to get a desired version:\n  " <> (lefts desiredVersions & T.intercalate "\n  ")
        Just v -> processValidDesiredVersion v
    processValidDesiredVersion v = do
      oldVersion <- getNodeVersionC
      let oldVerText = oldVersion <&> getNodeVersion & fromRight "?"
      envVarVal <- getEnvironmentVariableC envVarCmdName
      let cmdTempl = envVarVal & fromMaybe defaultChangeCommand & NodeManagerCommand
      changeRes <- changeNodeVersionC cmdTempl v
      case changeRes of
        Left e -> return $ Left e
        Right () -> processSuccessOfChangeCommand v oldVerText
    processSuccessOfChangeCommand v oldVerText = do
      newVersion <- getNodeVersionC
      case newVersion of
        Left e -> return $ Left e
        Right nv -> printFinalInfo v nv oldVerText
    printFinalInfo v nv oldVerText =
      let statusSign :: Text = if v == nv then "==" else "/="
          status :: Text = [qq|old = {T.unpack oldVerText} ... desired -> {getNodeVersion v} {statusSign} {getNodeVersion nv} <- current|]
       in if v == nv
            then do
              printC [qq|✓ Node version switched to {getNodeVersion nv}. ({status}) ⚙|]
              return $ Right ()
            else return $ Left [qq|❌ Version check failed: {status}|]

main :: IO ()
main = do
  setLocaleEncoding utf8
  program & versionFileToIO & nodeToIO & consoleToIO & environmentToIO & runM >>= exitWith
