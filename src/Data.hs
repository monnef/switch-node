{-# LANGUAGE DataKinds #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE PolyKinds #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeOperators #-}

module Data where

import Polysemy
import Utils

newtype NodeVersion = NodeVersion {getNodeVersion :: Text} deriving (Show, Eq)

newtype NodeManagerCommand = NodeManagerCommand {getNodeManagerCommand :: Text} deriving (Show, Eq)

data Console m a where
  PrintE :: Text -> Console m ()
  PrintErrorE :: Text -> Console m ()

makeSem ''Console

data VersionFile m a where
  ReadVersionFromVersionFileE :: Text -> VersionFile m (Either Text NodeVersion)

makeSem ''VersionFile

data Node m a where
  GetNodeVersionE :: Node m (Either Text NodeVersion)
  ChangeNodeVersionE :: NodeManagerCommand -> NodeVersion -> Node m (Either Text ())

makeSem ''Node

data Environment m a where
  GetEnvironmentVariableE :: Text -> Environment m (Maybe Text)

makeSem ''Environment
