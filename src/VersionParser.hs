{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}

module VersionParser where

import Control.Lens
import Control.Lens.Regex.Text
import Data
import qualified Data.Text as T
import Safe (headDef)
import Utils

parseNodeVersion :: Text -> Either Text NodeVersion
parseNodeVersion x = case m of
  [[r]] -> Right $ NodeVersion r
  _ -> Left $ "Failed to match version \"" <> x <> "\" (" <> tshow m <> ")"
  where
    m = x & T.lines & headDef "" & (^.. [regex|^v?(\d+\.\d+\.\d+)$|] . groups)
