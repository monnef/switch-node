{-# LANGUAGE DataKinds #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE PolyKinds #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeOperators #-}

module Interpreters where

import Control.Exception (SomeException, catch)
import qualified Control.Foldl as Fold
import Control.Foldl (fold)
import Data
import qualified Data.Text as T
import Polysemy
import Polysemy.Output
import System.Environment (getEnv)
import Turtle ((</>), ExitCode (..), FilePath, cat, fromText, input, isRegularFile, liftIO, lineToText, procStrictWithErr, pwd, shellStrictWithErr, stat, strict, testfile, view)
import Utils
import VersionParser (parseNodeVersion)
import Prelude hiding (FilePath)

consoleToIO :: Member (Embed IO) r => Sem (Console ': r) a -> Sem r a
consoleToIO = interpret $ \case
  PrintE msg -> embed $ tPutStrLn msg
  PrintErrorE msg -> embed $ tErrPutStrLn msg

nodeToIO :: Member (Embed IO) r => Sem (Node ': r) a -> Sem r a
nodeToIO = interpret $ \case
  GetNodeVersionE -> do
    (exitCode, stdOut, stdErr) <- liftIO $ procStrictWithErr "node" ["-v"] mempty
    return $ case exitCode of
      ExitSuccess -> stdOut & parseNodeVersion
      ExitFailure _ -> Left stdErr
  ChangeNodeVersionE cmdTempl v -> do
    let cmdStr = T.replace "{{version}}" (getNodeVersion v) (getNodeManagerCommand cmdTempl)
    (exitCode, stdOut, stdErr) <- liftIO $ shellStrictWithErr cmdStr mempty
    return $ case exitCode of
      ExitSuccess -> Right ()
      ExitFailure codeNum ->
        Left $ [[qq|Failed to run command {tshow cmdStr}, it returned {codeNum}.|], stdOut, stdErr] & T.intercalate "\n"

versionFileToIO :: Member (Embed IO) r => Sem (VersionFile ': r) a -> Sem r a
versionFileToIO = interpret $ \case
  ReadVersionFromVersionFileE fname -> do
    fileExists <- liftIO $ testfile (fromText fname)
    if fileExists
      then input (fromText fname) & strict & liftIO <&> parseNodeVersion
      else return $ Left $ "File \"" <> fname <> "\" doesn't exist."

environmentToIO :: Member (Embed IO) r => Sem (Environment ': r) a -> Sem r a
environmentToIO = interpret $ \case
  GetEnvironmentVariableE varName -> do
    val <- liftIO $ catch (getEnv (toS varName) <&> Just) (\(_ :: SomeException) -> return Nothing)
    return $ val <&> toS
