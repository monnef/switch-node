{-# LANGUAGE DataKinds #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE PolyKinds #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Commands where

import Data
import Polysemy
import Utils

printC :: Member Console r => Text -> Sem r ()
printC = printE

printErrorC :: Member Console r => Text -> Sem r ()
printErrorC = printErrorE

getNodeVersionC :: Member Node r => Sem r (Either Text NodeVersion)
getNodeVersionC = getNodeVersionE

changeNodeVersionC :: Member Node r => NodeManagerCommand -> NodeVersion -> Sem r (Either Text ())
changeNodeVersionC = changeNodeVersionE

readVersionFromVersionFileC :: Member VersionFile r => Text -> Sem r (Either Text NodeVersion)
readVersionFromVersionFileC = readVersionFromVersionFileE

getEnvironmentVariableC :: Member Environment r => Text -> Sem r (Maybe Text)
getEnvironmentVariableC = getEnvironmentVariableE
