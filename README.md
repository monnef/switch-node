# Switch node

Switch node is used to change NodeJS version using specific common Node version files like `.nvmrc`/`.node-version` in a current directory. The change will be handled by default by the [n](https://github.com/tj/n) command, but other node version managers can be used as well.

This program is a rewrite in Haskell of an older [bash script](https://gitlab.com/snippets/1899482). This version adds some features like verifying a current version after switching.

# Installation

Requirements:

* [Stack](https://haskellstack.org)

```sh
$ stack install
```

# Usage

```sh
$ cat .node-version
12.14.1
$ switch-node
⚙ Node version swapped. desired -> 12.14.1 == 12.14.1 <- current (old = 8.9.4)
```

## Other node version managers

You can use environmental variable `SWITCH_NODE_CMD` and `{{version}}` for a placeholder which will be replaced before running the command with a desired version.

```sh
$ SWITCH_NODE_CMD="echo V={{version}}; false" switch-node
❌ Error occurred:
Failed to run command "echo V=13.12.0; false", it returned 1.
V=13.12.0
```

So for a nvm it could be something like this:

```sh
$ SWITCH_NODE_CMD="nvm use {{version}}" switch-node
```

You can put the env. variable into your shell rc file (e.g. `.bashrc` or `.zshrc`):

```
export SWITCH_NODE_CMD="nvm use {{version}}"
```

# License
GPL-3
